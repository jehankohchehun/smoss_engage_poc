export class LookupResponseViewModel {
  public id: number;
  public name: string;
  public description: string;
  public others: string;
  public isDefault: number;
  public sortOrder: number;
  public others2: string;
}
