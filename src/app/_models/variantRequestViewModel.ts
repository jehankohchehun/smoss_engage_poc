export class VariantRequestViewModel {
  public SeriesDescription: string;
  public VariantId: number;
  public Source: string;
  public SourceId: string;
  public ReferenceId: string;
  public VGCode: string;
  public VehicleKey:string;

}
