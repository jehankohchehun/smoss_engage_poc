export class SeriesResponseViewModel {
  public name: string;
  public type: string;
  public imageString: string;
}
