
export class EKYCStatusResponseViewModel {
  public refNo: string;
  public calculationReferenceId: string;
  public bankruptcy: boolean;
  public oCR: string;
  public oCRRescan: string;
  public sMS: string;
  public securityQuestionFirstTry: string;
  public securityQuestionSecondTry: string;
}

export class EKYCStatusRequestViewModel {
  public refNo: string;
  public calculationId: number;
}
