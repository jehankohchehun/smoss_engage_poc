export class StateRequestViewModel {
  public DealerType: string;
  public Brand: string;
  public IsDealerGroup: boolean=false;
  public DealerId: number=0;

}
