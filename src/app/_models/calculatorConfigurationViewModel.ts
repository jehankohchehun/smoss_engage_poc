import { LookupResponseViewModel } from './lookupResponseViewModel';

export class CalculatorConfigurationRequest {
  public variantId: number;
  public queryParam: string;
  public source: string;
}
export class CalculatorConfigurationResponse {
  public stopApplyForFinancing: boolean;
  public source: string;
  public sourceId: string;
  public assetCost: number;
  public variantId: any;
  public installmentTypeToolTip: LookupResponseViewModel[];

}
