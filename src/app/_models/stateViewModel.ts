export class StateViewModel {
  public id: number;
  public name: string;
  public description: string;
}
