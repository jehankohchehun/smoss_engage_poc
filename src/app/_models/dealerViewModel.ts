export class DealerViewModel {
  public id: number;
  public name: string;
  public type: string;
  public subType: string;
  public category: string;
  public bankName: string;
  public accountNo: string;
  public branch: string;
  public addressType: string;
  public addressLine1: string;
  public addressLine2: string;
  public addressLine3: string;
  public state: string;
  public country: string;
}
