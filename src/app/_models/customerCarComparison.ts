import { CarComparison } from "./carComparison";

export class CustomerCarComparison {
  CalculationId: number;
  source: string = 'CFE';
  ProductType: number = 1;
  UserGuid: string;
  IsDeleted: number = 0;
  FullName: string;
  TitleId: number;
  DisplayName: string;
  EmailAddress: string;
  PhoneNumber: string;
  CountryId: number = 0;
  CountryCode: string = '';
  NationalID: string = '';
  DateOfBirth: string = '';
  PreferredDealerId: number;
  PreferredStateId: number;
  ReferenceId: string;
  RequestedURL: string;
  Key: string;
  SecretKey: string;
  ECalculatorDetail: CarComparison[];
  MarketingConsent: boolean;
  pdpaConsent: boolean;
  optionalPurposesConsent: boolean;
  saveType: string;
  proceedToEKYC: boolean;
  sourceId: string;
  steps: number;
}