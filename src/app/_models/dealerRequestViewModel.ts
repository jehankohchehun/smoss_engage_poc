export class DealerRequestViewModel {
  public DealerType: string;
  public Brand: string;
  public State: string;
  public IsDealerGroup: boolean=false;
  public DealerId: number=0;
  public StateId: number;
}
