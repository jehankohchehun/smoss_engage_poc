export class ResultViewModel {
  id: number;
  statusCode: number;
  status: string;
  isSucceed: boolean;
  message: string;
  messageDetail: string;
  data: any;
}