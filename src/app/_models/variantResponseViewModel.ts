export class VariantResponseViewModel {
  id: number;
  seriesDescription: string;
  variant: string;
  imageString: string;
  image_MimeType: string;
  isShopEnable: boolean;r  
}