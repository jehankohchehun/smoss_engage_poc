export class ECalculatorRequestViewModel {
  public variantId: number;
  public dPPercentage: number;
  public term: number;
  public mileage: number;
  public financingProduct: string;
  public firstFinancingProduct: string;
  public source: string
  public assetCost: number
  public assetType: string;
  public captchaToken: string;
  public dealer:string;
}
export class ECalculatorByIdRequestViewModel {
  public id: number;
  public searchText: string;
}