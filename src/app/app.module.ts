import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { MatTabsModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoaderComponent } from './_shared/loader/loader.component';
import { EmptyRouteComponent } from './empty-route/empty-route.component';
import { ECalculatorService } from "./_services/e-calculator.service";
import { DataService } from "./_services/data.service";
import { SecurityService } from "./_services/security.service";
import { LoaderService } from "./_services/loader.service";
import { ConfigurationDataService } from "./_services/configurationData.service";
import { HttpRequestInterceptor } from "./_security/http-interceptors";

import { NoSanitizePipe } from "./_tools/noSanitize-filter";

@NgModule({
  declarations: [
    AppComponent,
    LoaderComponent,
    EmptyRouteComponent,
    NoSanitizePipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    MatTabsModule,
    BrowserAnimationsModule,
    AppRoutingModule
  ],
  providers: [
    SecurityService,
    ECalculatorService,
    DataService,
    LoaderService,
    DatePipe,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpRequestInterceptor,
      multi: true
    },
    ConfigurationDataService,

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
