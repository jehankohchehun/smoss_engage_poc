import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { SeriesResponseViewModel } from "../_models/seriesResponseViewModel";
import { VariantRequestViewModel } from "../_models/variantRequestViewModel";
import { VariantResponseViewModel } from "../_models/variantResponseViewModel";
import { ECalculatorRequestViewModel, ECalculatorByIdRequestViewModel } from "../_models/eCalculatorRequestViewModel";
import { ECalculatorResponseViewModel, ECalculatorByIdResponseViewModel } from "../_models/eCalculatorResponseViewModel";
import { CustomerCarComparison } from "../_models/customerCarComparison";
import { ResultViewModel } from "../_models/resultViewModel";
import { LookupRequestViewModel } from "../_models/lookupRequestViewModel";
import { LookupResponseViewModel } from "../_models/lookupResponseViewModel";
import { DealerViewModel } from "../_models/dealerViewModel";
import { StateViewModel } from "../_models/stateViewModel";
import { StateRequestViewModel } from "../_models/stateRequestViewModel";
import { DealerRequestViewModel } from "../_models/dealerRequestViewModel";
import { SeriesTypeResponseViewModel } from "../_models/seriesTypeResponseViewModel";
import { SeriesRequestViewModel } from "../_models/seriesRequestViewModel";
import { EKYCStatusRequestViewModel, EKYCStatusResponseViewModel } from "../_models/ekycViewModel";
import { environment } from 'src/environment';
import { CalculatorConfigurationRequest } from '../_models/CalculatorConfigurationViewModel';
import { B1SQuotationRequestViewModel } from '../_models/B1SQuotationRequestViewModel';
import { CarComparison } from '../_models/carComparison';

const ecalculator = "ecalculator";
const b1s = "b1s";
const headers = new HttpHeaders().set('Content-Type', 'application/json').set('Access-Control-Allow-Origin', `${environment.UI_ENDPOINT}`);
 
@Injectable({
  providedIn: "root"
})
export class ECalculatorService {

  constructor(private http: HttpClient) { }

  getSeriesTypes(): Observable<SeriesTypeResponseViewModel[]> {
    return this.http.get<SeriesTypeResponseViewModel[]>(`${environment.NFSC_ENDPOINT}${b1s}/getSeriesTypes`);
  }

  getSeries(selectedSeriesType: SeriesRequestViewModel): Observable<SeriesResponseViewModel[]> {
    return this.http.post<SeriesResponseViewModel[]>(`${environment.NFSC_ENDPOINT}${b1s}/getSeries`, selectedSeriesType, { headers });
  }

  getVariants(selectedSeries: VariantRequestViewModel): Observable<VariantResponseViewModel[]> {
    return this.http.post<VariantResponseViewModel[]>(`${environment.NFSC_ENDPOINT}${b1s}/GetVariant`, selectedSeries, { headers });
  }

  getVariantImage(selectedSeries: VariantRequestViewModel): Observable<VariantResponseViewModel> {
    return this.http.post<VariantResponseViewModel>(`${environment.NFSC_ENDPOINT}${b1s}/GetVariantById`, selectedSeries, { headers });
  }

  getVariantIdbyVGCode(selectedSeries: VariantRequestViewModel): Observable<VariantResponseViewModel[]> {
    return this.http.post<VariantResponseViewModel[]>(`${environment.NFSC_ENDPOINT}${b1s}/GetVariantByVehicleKey`, selectedSeries, { headers });
  }

  getVariantWOImage(selectedSeries: VariantRequestViewModel): Observable<VariantResponseViewModel[]> {
    return this.http.post<VariantResponseViewModel[]>(`${environment.NFSC_ENDPOINT}${b1s}/GetVariantWOImage`, selectedSeries, { headers });
  }

  getInstallment(selectedVariant: ECalculatorRequestViewModel): Observable<ECalculatorResponseViewModel> {
    return this.http.post<ECalculatorResponseViewModel>(`${environment.NFSC_ENDPOINT}${b1s}/GetInstallment`, selectedVariant, { headers });
  }

  getUCVehicleInfo(Source: string, CalculationReferenceId: string, VehicleInfoReferenceId: string): Observable<any> {
    var params = new HttpParams().set('Source', Source).set('VehicleInfoReferenceId', VehicleInfoReferenceId).set('CalculationReferenceId', CalculationReferenceId);
    return this.http.get<any>(`${environment.CFE_ENDPOINT}${ecalculator}/GetUCVehicleInfoByReferenceId`, { params });
  }

  getECalculatorById(id: ECalculatorByIdRequestViewModel): Observable<ECalculatorByIdResponseViewModel[]> {
    return this.http.post<ECalculatorByIdResponseViewModel[]>(`${environment.CFE_ENDPOINT}${ecalculator}/GetECalculatorById`, id, { headers });
  }

  getLookup(request: LookupRequestViewModel): Observable<LookupResponseViewModel[]> {
    if (request.source)
      request.source = `${environment.Source}`;
    return this.http.post<LookupResponseViewModel[]>(`${environment.NFSC_ENDPOINT}${b1s}/GetLookup`, request, { headers });
  }

  getCFELookup(request: LookupRequestViewModel): Observable<LookupResponseViewModel[]> {
    if (request.source)
      request.source = `${environment.Source}`;
    return this.http.post<LookupResponseViewModel[]>(`${environment.CFE_ENDPOINT}${ecalculator}/GetLookup`, request, { headers });
  }

  getCFELookupHTML(request: LookupRequestViewModel): Observable<LookupResponseViewModel[]> {
    if (request.source)
      request.source = `${environment.Source}`;
    return this.http.post<LookupResponseViewModel[]>(`${environment.CFE_ENDPOINT}${ecalculator}/GetLookupHTML`, request, { headers });
  }

  getStateByDealerType(selectedDealerType: StateRequestViewModel): Observable<StateViewModel[]> {
    return this.http.post<StateViewModel[]>(`${environment.NFSC_ENDPOINT}${b1s}/GetStateByDealerType`, selectedDealerType, { headers });
  }

  getDealerByState(selectedState: DealerRequestViewModel): Observable<DealerViewModel[]> {
    return this.http.post<DealerViewModel[]>(`${environment.NFSC_ENDPOINT}${b1s}/GetDealerByState`, selectedState, { headers });
  }

  getEKYCStatusByReferenceNo(ekycStatusRequest: EKYCStatusRequestViewModel): Observable<EKYCStatusResponseViewModel> {
    return this.http.post<EKYCStatusResponseViewModel>(`${environment.CFE_ENDPOINT}${ecalculator.replace("ecalculator", "ekyc")}/GetEKYCStatusByReferenceNo`, ekycStatusRequest, { headers });
  }

  saveCustomerCarComparison(customerCarComparison: CustomerCarComparison): Observable<ResultViewModel> {
    return this.http.post<ResultViewModel>(`${environment.CFE_ENDPOINT}${ecalculator}/SaveECalculator`, customerCarComparison, { headers });
  }
  SaveB1SCalculator(model: B1SQuotationRequestViewModel): Observable<ResultViewModel> {
    return this.http.post<ResultViewModel>(`${environment.CFE_ENDPOINT}${ecalculator}/SaveB1SCalculator`, model, { headers });
  }
  getCalculatorConfiguration(model: CalculatorConfigurationRequest): Observable<ResultViewModel> {
    return this.http.post<ResultViewModel>(`${environment.CFE_ENDPOINT}${ecalculator}/GetECalculatorConfiguration`, model, { headers });
  }

  getRandom(length) {
    return Math.floor(
      Math.pow(10, length - 1) + Math.random() * 9 * Math.pow(10, length - 1)
    );
  }

  PopulateCalculatorDetails(row: ECalculatorByIdResponseViewModel) {
    const calculatorDetailResponse = new ECalculatorResponseViewModel();
    calculatorDetailResponse.financingProduct = row.financingProduct;
    calculatorDetailResponse.campaignId = row.campaignId;
    calculatorDetailResponse.campaign = row.campaign;

    calculatorDetailResponse.campaignModelId = row.campaignModelId;
    calculatorDetailResponse.campaignExpiryDate = null;
    calculatorDetailResponse.validityDate = null;
    calculatorDetailResponse.rate = row.rate;
    calculatorDetailResponse.rrp = row.rrp;
    calculatorDetailResponse.gfv = row.gfv;
    calculatorDetailResponse.rv = row.rv;
    calculatorDetailResponse.monthlyInstallment = row.monthlyInstallment;
    calculatorDetailResponse.lastMonthInstallment = row.lastInstallment;
    calculatorDetailResponse.dpPercentage = row.downPaymentPercentage;
    calculatorDetailResponse.downPayment = row.downPayment;
    calculatorDetailResponse.dpPercentageMin = row.dPPercentageMin;
    calculatorDetailResponse.dpPercentageMax = row.dPPercentageMax;
    calculatorDetailResponse.dpPercentageInterval = row.dPPercentageInterval;
    calculatorDetailResponse.term = row.tenure;
    calculatorDetailResponse.termMin = row.termMin;
    calculatorDetailResponse.termMax = row.termMax;
    calculatorDetailResponse.termInterval = row.termInterval;

    if (row.financingProduct === "EasyDrive") {
      calculatorDetailResponse.mileage = row.mileage;
    } else {
      calculatorDetailResponse.mileage = 0;
    }
    calculatorDetailResponse.mileageMin = row.mileageMin;
    calculatorDetailResponse.mileageMax = row.mileageMax;
    calculatorDetailResponse.mileageInterval = row.mileageInterval;

    calculatorDetailResponse.straightLine = row.straightLine;
    calculatorDetailResponse.balloon = row.balloon;
    calculatorDetailResponse.easyDrive = row.easyDrive;
    calculatorDetailResponse.straightLineDescription = row.straightLineDescription;
    calculatorDetailResponse.balloonDescription = row.balloonDescription;
    calculatorDetailResponse.easyDriveDescription = row.easyDriveDescription;
    calculatorDetailResponse.variantId = row.variantId;
    calculatorDetailResponse.validityDate = row.validityDate;
    calculatorDetailResponse.campaignExpiryDate = row.campaignExpiryDate;
    calculatorDetailResponse.assetCost = row.assetCost;
    calculatorDetailResponse.amountFinance = row.amountFinance;
    calculatorDetailResponse.chassisNumber = row.chassisNumber;
    calculatorDetailResponse.registrationNumber = row.registrationNumber;
    calculatorDetailResponse.assetType = row.assetType;
    return calculatorDetailResponse;
  }
  PopulateSelectVariant(row: ECalculatorByIdResponseViewModel) {
    const selectedVariantResponse = new VariantResponseViewModel();
    selectedVariantResponse.id = row.variantId;
    selectedVariantResponse.variant = row.customerDescription;
    selectedVariantResponse.seriesDescription = row.seriesDescription;
    //selectedVariantResponse.imageString = row.imageString;
    //selectedVariantResponse.image_MimeType = row.image_MimeType;
    return selectedVariantResponse;
  }
  PopulatePersonalDetail(row: ECalculatorByIdResponseViewModel, uniqueId: string) {
    const customer = new CustomerCarComparison();
    customer.CalculationId = row.id;
    customer.source = row.source;
    customer.sourceId = row.sourceId;
    customer.ReferenceId = uniqueId;
    customer.PreferredStateId = row.preferredStateId;
    customer.PreferredDealerId = row.preferredDealerId;
    customer.CalculationId = row.eCalculatorId;
    customer.TitleId = row.customerTitleId;
    customer.FullName = row.customerName;
    customer.DisplayName = row.customerDisplayName;
    customer.CountryId = row.customerCountryId;
    customer.PhoneNumber = row.customerPhoneNumber.replace(/^0+/, "");
    customer.NationalID = row.customerNationalID;
    customer.CountryCode = row.customerCountryCode;
    customer.EmailAddress = row.customerEmailAddress;
    customer.MarketingConsent = row.marketingConsent;
    customer.UserGuid = "";
    customer.RequestedURL = `ECalculatorPersonalDetail`;
    return customer;
  }
  PopulateCarComparison(row: ECalculatorByIdResponseViewModel, uniqueId: string) {
    const carComparison = new CarComparison();
    carComparison.UniqueId = uniqueId === undefined ? "" : uniqueId;
    carComparison.OptionNo = 1;
    carComparison.VariantId = row.variantId;
    carComparison.FinancingProduct = row.financingProduct;
    carComparison.RRP = row.rrp;
    carComparison.DownPaymentPercentage = row.downPaymentPercentage;
    carComparison.DownPayment = row.downPayment;
    carComparison.Tenure = row.tenure;
    carComparison.Mileage = row.mileage = row.financingProduct === "EasyDrive" ? row.mileage : 0;
    carComparison.MonthlyInstallment = row.monthlyInstallment
    carComparison.LastInstallment = row.lastInstallment
    carComparison.GFV = row.gfv
    carComparison.CalculationId = row.eCalculatorId === undefined ? 0 : row.eCalculatorId;
    carComparison.FullName = row.customerName === undefined ? "" : row.customerName;
    carComparison.EmailAddress = row.email === undefined ? "" : row.email;
    carComparison.PhoneNumber = row.customerPhoneNumber === undefined ? "" : row.customerPhoneNumber.replace(/^0+/, "");
    carComparison.PreferredDealerId = row.preferredDealerId;
    carComparison.PreferredStateId = row.preferredStateId;
    carComparison.RequestedURL = `Configurator`;
    carComparison.MarketingConsent = row.marketingConsent === undefined ? false : row.marketingConsent;
    carComparison.CampaignModelId = row.campaignModelId
    carComparison.CampaignExpiryDate = row.campaignExpiryDate;
    carComparison.ValidityDate = row.validityDate
    carComparison.InterestRate = row.rate === undefined ? row.interestRate : row.rate;
    carComparison.AssetCost = row.assetCost
    carComparison.AmountFinance = row.amountFinance
    return carComparison;
  }
}