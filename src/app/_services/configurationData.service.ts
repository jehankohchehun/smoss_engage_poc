import { Injectable } from '@angular/core';
import { ECalculatorResponseViewModel } from '../_models/eCalculatorResponseViewModel';
import { VariantResponseViewModel } from '../_models/variantResponseViewModel';
import { CarComparison } from '../_models/carComparison';
import { CustomerCarComparison } from '../_models/customerCarComparison';

@Injectable()
export class ConfigurationDataService {
  public calculationId: number;
  public referenceId: string;
  public vreferenceId: string;
  public stage: number;
  public selectedVariant: VariantResponseViewModel;
  public calculatorDetail: ECalculatorResponseViewModel;
  public carComparison: CarComparison;
  public backFromQuotationSummary: boolean = false;
  public CustomerCarComparison: CustomerCarComparison;
  public stopApplyForFinancing: boolean;
  public source: string;
  public sourceId: string;
  public referenceNo: string;
  isFromUCCompare: boolean = false;
  vehicleInfo: any;
  public constructor() { }
}