import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { tap } from "rxjs/operators";
import { AppUserAuth } from "../_security/app-user-auth";
import { TokenRequestViewModel } from "../_security/tokenRequestViewModel";
import { TokenResponseViewModel } from "../_security/tokenResponseViewModel";
import { RefreshTokenViewModel } from "../_security/RefreshTokenViewModel";
import { environment } from 'src/environment';

const SecurityToken = "SecurityToken";
const security = "security";
const headers = new HttpHeaders().set('Content-Type', 'application/json').set('Access-Control-Allow-Origin', '*');

@Injectable()
export class SecurityService {
  securityObject: AppUserAuth = new AppUserAuth();
  tokenReqViewModel: TokenRequestViewModel = new TokenRequestViewModel();

  constructor(private http: HttpClient) { }

  updateJWTToken(reqSource: string, isUnauthorized: boolean = false) {
    return new Promise(resolve => {
      let isTokenExpired = true;
      if (!isUnauthorized) {
        isTokenExpired = this.isTokenExpired(reqSource);
      }

      if (!isTokenExpired) {
        resolve(true);
      } else {
        this.getNewJwtToken(reqSource).subscribe(resp => {
          if (resp) {
            resolve(true);
          } else {
            resolve(false);
          }
        });
      }
    });
  }

  isTokenExpired(reqSource: string, token?: string): boolean {
    if (!reqSource) {
      return true;
    } else {
      if (token == null)
        token = this.getJwtToken(reqSource);
      if (token == null) return true;

      const claims = JSON.parse(atob(token.split(".")[1]));
      const date = new Date(claims.exp * 1000);
      if (date === undefined) { return false; }
      return !(date.valueOf() > new Date().valueOf());
    }
  }

  getTokenExpiryTime(reqSource: string, token?: string): Date {
    if (!reqSource) {
      return null;
    } else {
      if (token == null)
        token = this.getJwtToken(reqSource);
      const claims = JSON.parse(atob(token.split(".")[1]));
      const date = new Date(claims.exp * 1000);
      return date;
    }
  }

  getNewJwtToken(reqSource: string): Observable<TokenResponseViewModel> {

    let apiSourceUrl = "";
    if (reqSource === "CFE") {
      apiSourceUrl = `${environment.CFE_ENDPOINT}`;
    } else if (reqSource === "NFSC") {
      apiSourceUrl = `${environment.NFSC_ENDPOINT}`;
    }
    else if (reqSource === "SSP") {
      apiSourceUrl = `${environment.CFE_ENDPOINT}`;

      const refreshTokenViewModel = new RefreshTokenViewModel();
      refreshTokenViewModel.Token = this.getJwtToken("SSP");
      refreshTokenViewModel.RefreshToken = this.getRefreshToken("SSP");
      return null;
    }
    else {
      return null;
    }
    this.resetSecurityObject(reqSource);
    const tokenRequestViewModel = new TokenRequestViewModel();
    tokenRequestViewModel.Source = `CFE`;

    return this.http.post<TokenResponseViewModel>(`${apiSourceUrl}${SecurityToken}/getToken`, tokenRequestViewModel, { headers })
      .pipe(
        tap(resp => {
          Object.assign(this.securityObject, resp);
          this.saveJwtToken(reqSource, this.securityObject.bearerToken);
          this.saveRefreshToken(reqSource, this.securityObject.refreshToken);
        })
      );
  }

  getNFSCToken() {
    this.resetSecurityObject("nfsccore");
    this.tokenReqViewModel.Source = `CFE`;

    return this.http.post<TokenResponseViewModel>(`${environment.NFSC_ENDPOINT}${SecurityToken}/getToken`, this.tokenReqViewModel, { headers }
    )
      .pipe(
        tap(resp => {
          Object.assign(this.securityObject, resp);
          localStorage.setItem("bearerToken", this.securityObject.bearerToken);
        })
      );
  }

  getJwtToken(reqSource: string): string {
    if (reqSource === "NFSC") {
      return localStorage.getItem("bearerToken");
    } else if (reqSource === "CFE") {
      return localStorage.getItem("bearerTokenCFE");
    }
    else if (reqSource === "SSP") {
      return localStorage.getItem("bearerTokenSSP");
    }
    else {
      return null;
    }
  }

  getRefreshToken(reqSource: string) {
    if (reqSource === "NFSC") {
      return localStorage.getItem("nfscRefreshToken");
    } else if (reqSource === "CFE") {
      return localStorage.getItem("cfeRefreshToken");
    }
    else if (reqSource === "SSP") {
      return localStorage.getItem("sspRefreshToken");
    }
  }

  saveRefreshToken(reqSource: string, refreshToken: any) {
    if (reqSource === "NFSC") {
      return localStorage.setItem("nfscRefreshToken", refreshToken);
    } else if (reqSource === "CFE") {
      return localStorage.setItem("cfeRefreshToken", refreshToken);
    }
    else if (reqSource === "SSP") {
      return localStorage.setItem("sspRefreshToken", refreshToken);
    }
  }

  saveJwtToken(reqSource: string, token: string) {
    if (reqSource === "NFSC") {
      return localStorage.setItem("bearerToken", token);
    } else if (reqSource === "CFE") {
      return localStorage.setItem("bearerTokenCFE", token);
    }
    else if (reqSource === "SSP") {
      return localStorage.setItem("bearerTokenSSP", token);
    }
  }

  // SSP Starts
  removeLocalstorageItems() {
    // localStorage.removeItem("bearerTokenSSP");
    // localStorage.removeItem("AppUserAuth");
    sessionStorage.clear();
    localStorage.clear();
  }
  resetSecurityObject(reqSource: string = "CFE"): void {
    this.securityObject.userName = "";
    this.securityObject.bearerToken = "";
    this.securityObject.isAuthenticated = false;
    if (reqSource === "NFSC") {
      localStorage.removeItem("bearerToken");
      localStorage.removeItem("nfscRefreshToken");
    } else if (reqSource === "CFE") {
      localStorage.removeItem("bearerTokenCFE");
      localStorage.removeItem("cfeRefreshToken");
    }
  }
}
