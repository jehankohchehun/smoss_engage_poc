import { Injectable } from "@angular/core";
import { Observable, of, forkJoin, Subject, pipe } from "rxjs";
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpHeaders, HttpErrorResponse } from "@angular/common/http";
import { catchError, finalize, switchMap } from "rxjs/operators";
import { Router, ActivatedRoute } from "@angular/router";

import { SecurityService } from "../_services/security.service";
import { LoaderService } from "../_services/loader.service";
import { environment } from 'src/environment';

@Injectable({
  providedIn: 'root'
})
export class HttpRequestInterceptor implements HttpInterceptor {
  // tslint:disable-next-line:variable-name
  private _refreshToken: Subject<any> = new Subject<any>();

  constructor(private router: Router,
    private activatedRoute: ActivatedRoute,
    private loaderService: LoaderService,
    private securityService: SecurityService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this.loaderService.show();
    const reqSource = this.getRequestedSource(req.url);

    let token = ""; // "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIiLCJqdGkiOiJkNjc1MjlkOC04ZjQwLTQwNjctODhmOC05MWI5MmVkZWJkOTkiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoiIiwibmJmIjoxNTk3ODIyMzI2LCJleHAiOjE1OTc4MjIzODYsImlzcyI6Imh0dHBzOi8vd3d3LmJtd2ZzLm15LyIsImF1ZCI6IkNGRVVzZXJzIn0.jOO-YGhzyDbmg9M0P6DTZyUOQfhHPKKkHVJb2shy8to";
    token = this.securityService.getJwtToken(reqSource);
    const newReq = null;
    if (token) {
      const authToken = "Bearer " + token;
      const newReq = req.clone({
        headers: new HttpHeaders({
          "Content-Type": "application/json",
          "Access-Control-Allow-Origin": `${environment.UI_ENDPOINT}`,
          "Set-Cookie": "HttpOnly;Secure",
          Authorization: authToken
        })
      });
      return next.handle(newReq).pipe(
        finalize(() => this.loaderService.hide()),
        catchError((error, caught) => {
          if (error.status === 401 && reqSource != 'SSP') {

            return this.updateToken(reqSource).pipe(
              switchMap(() => {
                return next.handle(this.updateHeader(req, reqSource)) as any;
              }),
              catchError((err, ct) => {
                this.handleAuthError(err, reqSource);
                return of(error);
              })
            );
          } else {
            this.handleAuthError(error, reqSource);
          }
          return of(error);
        }) as any
      );
    } else {
      const newReq = req.clone({
        headers: new HttpHeaders({
          "Content-Type": "application/json",
          "Access-Control-Allow-Origin": `${environment.CFE_ENDPOINT}`,
          "Set-Cookie": "HttpOnly;Secure"
        })
      });
      return next.handle(newReq).pipe(
        finalize(() => this.loaderService.hide()),
        catchError((error, caught) => {
          this.handleAuthError(error);
          return of(error);
        }) as any
      );
    }
  }
  updateHeader(req, reqSource) {
    const newAuthTokens = this.securityService.getJwtToken(reqSource);
    const newHeaders = new HttpHeaders({
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": `${environment.UI_ENDPOINT}`,
      "Set-Cookie": "HttpOnly;Secure",
      Authorization: `Bearer ${newAuthTokens}`
    });
    req = req.clone({
      headers: newHeaders
    });
    return req;
  }
  private updateToken(reqSource) {
    this._refreshToken.subscribe({
      complete: () => {
        this._refreshToken = new Subject<any>();
      }
    });
    if (this._refreshToken.observers.length === 1) {
      this.securityService.getNewJwtToken(reqSource).subscribe(this._refreshToken);
    }
    return this._refreshToken;
  }
  getRequestedSource(url: string) {
    if (url.includes('StoreLog')) {
      return "UILog";
    }
    else if (this.router.url.includes('/SSP/') && !url.includes("api/emandate/SendTac")) {
      return "SSP";
    }
    else if (url.includes(`${environment.NFSC_ENDPOINT}`)) {
      return "NFSC";
    } else if (
      url.includes(`${environment.CFE_ENDPOINT}`) &&
      !url.includes("api/emandate/SendTac")
    ) {
      return "CFE";
    } else {
      return "CFE";
    }
  }

  /**
   * manage errors
   * @param err
   * @returns {any}
   */
  private handleAuthError(err: HttpErrorResponse, Source: string = null): Observable<any> {
    let MessageHeader = "";
    let MessageDetails = "";
    let RedirectTo = null
    this.loaderService.hide();
    //here
    if (err.status === 200 || err.url.toLowerCase().includes('/logout')) {
      return;
    }
    if (err.status === 203) {
      this.router.navigate(["/emandate/expired-link"], {
        relativeTo: this.activatedRoute
      });
      return of(err.message);
    }
    if (MessageHeader == '') {

      if (err.status === 401 && Source == "SSP") {
        MessageHeader = "Login Message";
        MessageDetails = "Your session has expired due to inactivity. Please log in again.";
      }
      else if (err.status === 403 && Source == "SSP") {
        MessageHeader = "Session Ended";
        MessageDetails = "This session has ended due to another active session. Please ensure you have logged out of any active session and proceed to login again. For further assistance please contact BMW Customer Relations at 1800 88 300.";
      }
      else if (err.status == 0 || err.status == 504) {
        MessageHeader = "Network Unstable";
        MessageDetails = "Connection to our server was lost due to an unstable network connection. Please ensure you have sufficient WiFi or Data coverage and try again. If the issue persists contact our Customer Interaction Centre at +603-27192688 for further assistance.";
      }
      else {
        MessageHeader = "Request Timeout";
        MessageDetails = "Your session or connection to our server may have timed out. Please refresh the page and try again. If the issue persists contact our Customer Interaction Centre at +603-27192688 for further assistance.";
        let path = window.location.pathname.split('/')[1];
        if (path === 'SSP' || path === 'authentication')
          RedirectTo = '/authentication/login';

      }

    }
    let messageData = {
      Status: MessageHeader,
      Message: MessageDetails,
      RedirectTo: RedirectTo,
      Others: null
    };
    this.loaderService.hide();

    return of(err.message);
  }
}
