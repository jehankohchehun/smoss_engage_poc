export class AppUserAuth {
  userName: string = "";
  bearerToken: string = "";
  updateInfo: string = "";
  isAuthenticated: boolean = false;
  userGuid: string;
  message: string;
  messageDetails: string;
  refreshToken: string = "";
  displayName: string = "";
  inboxCount = 0
  inbox: any[];
}