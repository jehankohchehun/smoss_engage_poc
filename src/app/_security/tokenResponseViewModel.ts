export class TokenResponseViewModel {
  public userName: string;
  public bearerToken: string;
  public refreshToken: string;

  public isAuthenticated: boolean;
  public message: string;
  public messageDetails: string;

  public claims: string;
}
