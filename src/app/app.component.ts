import { Component, EventEmitter, OnInit, OnDestroy } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { forkJoin, Subscription } from "rxjs";
import { singleSpaPropsSubject, SingleSpaProps } from 'src/single-spa/single-spa-props';

import { VariantResponseViewModel } from "./_models/variantResponseViewModel";
import { VariantRequestViewModel } from "./_models/variantRequestViewModel";
import { ECalculatorResponseViewModel } from "./_models/eCalculatorResponseViewModel";
import { SeriesResponseViewModel } from "./_models/seriesResponseViewModel";
import { SeriesTypeResponseViewModel } from "./_models/seriesTypeResponseViewModel";
import { SeriesRequestViewModel } from "./_models/seriesRequestViewModel";
import { LookupResponseViewModel } from './_models/lookupResponseViewModel';
import { CustomerCarComparison } from "./_models/customerCarComparison";
import { CarComparison } from "./_models/carComparison";
import { ResultViewModel } from "./_models/resultViewModel";
import { CalculatorConfigurationRequest, CalculatorConfigurationResponse } from './_models/CalculatorConfigurationViewModel';
import { ECalculatorRequestViewModel, ECalculatorByIdRequestViewModel } from "./_models/eCalculatorRequestViewModel";

import { ECalculatorService } from "./_services/e-calculator.service";
import { ConfigurationDataService } from "./_services/configurationData.service";
import { DataService } from "./_services/data.service";
import { SecurityService } from "./_services/security.service";


@Component({
  selector: 'app2-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {  
  singleSpaProps: SingleSpaProps;
  subscription: Subscription;

  title = 'app2';
  source: string = '';
  AssetCost: number = 0;
  selection: string = ""
  id: number;
  selected: boolean = false;
  openedMobile: boolean = false;
  currentStep: string;
  dpValue: number = 0;
  tenureValue: number = 0;
  selectedFinancingProduct = "StraightLine";
  selectedMileage;
  assetType: string = "New";
  opened: boolean = false;
  VGCode: string;
  variantId = 0;
  uniqueId: string = "";
  stopApplyForFinancing: boolean = false;
  queryParam: string;
  eCalculatorId: number = 0;
  name: string;
  email: string;
  phoneNumber: string;
  userGuid: string = "";
  marketingConsent = false;
  PreferredDealerId: number;
  PreferredStateId: number;
  variantTextD: string = '';
  variantTextM: string = '';
  customerName: string = '';
  customerEmail: string = '';
  customerMobileNumber: string = '';

  //API models
  resultViewModel: ResultViewModel;
  calculatorDetail: ECalculatorResponseViewModel;
  eCalcByIdReq: ECalculatorByIdRequestViewModel;
  carTabVariants: VariantResponseViewModel[] = [];
  selectedVariant: VariantResponseViewModel;
  seriesDetail: SeriesResponseViewModel[];
  seriesTypes: SeriesTypeResponseViewModel[] = [];
  selectedSeriesDetail: SeriesResponseViewModel[];
  
  //tool tip information
  easyDriveInformation1: string;
  balloonInformation1: string;
  straightLineInformation1: string;
  easyDriveInformation2: string;
  balloonInformation2: string;
  straightLineInformation2: string;

  constructor(
    private activatedRoute: ActivatedRoute,
    private eCalculatorService: ECalculatorService,
    private data: DataService,
    private securityService: SecurityService,
    private configurationDataService: ConfigurationDataService) { }
  
  ngOnInit() {  
    this.subscription = singleSpaPropsSubject.subscribe(
      props => (this.singleSpaProps = props),
    );

    this.VGCode = this.singleSpaProps["VGCode"];
    console.log(this.VGCode);
    this.activatedRoute.queryParams.subscribe(params => {
      if (this.activatedRoute.snapshot.queryParams.VGCode) {
        this.VGCode = encodeURIComponent(params.VGCode);
      }

      forkJoin([
        this.securityService.updateJWTToken("CFE"),
        this.securityService.updateJWTToken("NFSC")
      ]).subscribe(([cfeResp, nfscResp]) => {
        if (cfeResp && nfscResp) {
          this.bindSeries();
        }
      });
    });
  }
  
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  bindSeries() {
    this.seriesTypes = [];
    // const map = new Map();
    this.getSeriesTypes().then(() => {
      if (this.seriesTypes.length > 0) {
        this.getSeries(this.seriesTypes[0].name).then(() => {
          if (this.selectedSeriesDetail.length > 0) {
            this.getCarVariants(this.seriesTypes[0].name).then(() => {
              this.getCalculatorConfiguration().then(() => {
                if (this.variantId > 0 || (this.VGCode != null && this.VGCode != ""))  // For shop.bmw.com.my // Added by Ahsan
                {
                  this.getVariantIdByVGCode().then(() => {
                    if (this.variantId > 0) {
                      this.getSelectedVariantImage(this.variantId).then(() => {
                        if (this.selectedVariant.id > 0)
                          this.select(this.selectedVariant);
                      });
                    }
                  })
                }
              });
            });
          }
        });
      }
    });
  }

  getSeriesTypes() {
    return new Promise<void>(resolve => {
      this.eCalculatorService.getSeriesTypes().subscribe(carSeriesTypes => {
        this.seriesTypes = carSeriesTypes;
        resolve();
      });
    });
  }

  getSeries(type: string) {
    const seriesRequest = new SeriesRequestViewModel();
    seriesRequest.seriesType = type;
    return new Promise<void>(resolve => {
      this.eCalculatorService.getSeries(seriesRequest).subscribe(carSeries => {
        this.selectedSeriesDetail = carSeries;
        resolve();
      });
    });
  }

  getCarVariants(seriesName: string) {
    let variantRequestViewModel: VariantRequestViewModel = new VariantRequestViewModel();
    variantRequestViewModel.SeriesDescription = seriesName;
    return new Promise<void>(resolve => {
      this.eCalculatorService
        .getVariants(variantRequestViewModel)
        .subscribe(carVariants => {
          if (carVariants) {
            this.carTabVariants = carVariants;
          } else {
            this.carTabVariants = [];
          }
          resolve();
        });
    });
  }
  
  getCalculatorConfiguration() {
    var model = new CalculatorConfigurationRequest();
    model.variantId = this.variantId;
    model.queryParam = this.queryParam;
    model.source = this.source;

    return new Promise<void>(resolve => {
      this.eCalculatorService.getCalculatorConfiguration(model).subscribe(resp => {
        if (resp.isSucceed) {
          if (resp.data != null) {
            let data = <CalculatorConfigurationResponse>resp.data;
            if (data.stopApplyForFinancing)
              this.stopApplyForFinancing = true;
            if (data.source)
              this.configurationDataService.source = data.source;
            if (data.sourceId)
              this.configurationDataService.sourceId = data.sourceId;
            if (data.assetCost)
              this.AssetCost = data.assetCost;
            if (data.variantId)
              this.variantId = data.variantId;
            this.bindInstallmentTypeToolTip(data.installmentTypeToolTip);
          }
        }
        resolve();
      });
    });
  }

  bindInstallmentTypeToolTip(lookupRes: LookupResponseViewModel[]) {
    if (lookupRes != null && lookupRes.length > 0) {
      this.easyDriveInformation1 = lookupRes.filter(p => p.name == "EasyDrive1")[0].description;
      this.easyDriveInformation2 = lookupRes.filter(p => p.name == "EasyDrive2")[0].description;
      this.balloonInformation1 = lookupRes.filter(p => p.name == "Balloon1")[0].description;
      this.balloonInformation2 = lookupRes.filter(p => p.name == "Balloon2")[0].description;
      this.straightLineInformation1 = lookupRes.filter(p => p.name == "StraightLine1")[0].description;
      this.straightLineInformation2 = lookupRes.filter(p => p.name == "StraightLine2")[0].description;
    }
  }
  
  bindCalculatorData(selectedVariantResponse: VariantResponseViewModel, calculatorDetailResponse: ECalculatorResponseViewModel) {
    // Get the selected Variant for CAR 1
    this.selectedVariant = selectedVariantResponse;
    this.calculatorDetail = calculatorDetailResponse;

    // if (this.selectedVariant.imageString) {
    //   this.carImage = this.transform(this.selectedVariant.imageString);
    // }
    this.selection = this.selectedVariant.variant;
    this.dpValue = this.calculatorDetail.dpPercentage;

    // if (this.calculatorDetail.variantDisplayText1 != undefined || this.calculatorDetail.variantDisplayText2 != undefined) {
    //   this.variantTextM = this.t$.data.configCampaignM.replace('{0}', this.calculatorDetail.variantDisplayText1).replace('{1}', this.calculatorDetail.variantDisplayText2);
    //   this.variantTextD = this.t$.data.configCampaignD.replace('{0}', this.calculatorDetail.variantDisplayText1).replace('{1}', this.calculatorDetail.variantDisplayText2);
    // }

    this.tenureValue = this.calculatorDetail.term;
    this.selectedFinancingProduct = this.calculatorDetail.financingProduct;

    this.id = this.selectedVariant.id;
    this.selected = true;
    this.opened = false;
    this.openedMobile = false;
    this.financeStep();
  }

  getVariantIdByVGCode() {    
    const variantRequestViewModel = new VariantRequestViewModel();
    variantRequestViewModel.VGCode = this.VGCode;
    return new Promise<void>(resolve => {
      this.eCalculatorService.getVariantIdbyVGCode(variantRequestViewModel).subscribe(resp => {
        if (resp != null && resp.length > 0)
          this.variantId = resp[0].id;
        resolve();
      });
    });
  }

  getSelectedVariantImage(id: number) {
    const variantRequestViewModel: VariantRequestViewModel = new VariantRequestViewModel();
    variantRequestViewModel.VariantId = id;
    variantRequestViewModel.Source = this.source;
    // variantRequestViewModel.ReferenceId = this.configurationDataService.referenceId;
    return new Promise<void>(resolve => {
      this.eCalculatorService
        .getVariantImage(variantRequestViewModel)
        .subscribe(carVariants => {
          if (carVariants) 
            this.selectedVariant = carVariants;
          else
            this.selectedVariant.imageString = "";
          resolve();
        });
    });
  }

  public select(selectedVariant: VariantResponseViewModel, resetAssetcost = false) {
    this.getSelectedVariantImage(selectedVariant.id).then(() => { });
    this.selectedVariant = selectedVariant;
    if (resetAssetcost)
      this.AssetCost = 0;
    this.populateCalculatorDetail('select', true);

    this.selection = selectedVariant.variant;
    this.id = selectedVariant.id;
    this.selected = true;
    this.opened = false;
    this.openedMobile = false;
    this.financeStep();
  }  

  populateCalculatorDetail(action: string, isDefault: boolean = true) {
    this.getECalculatorDetail('', isDefault).then(() => {
      this.dpValue = this.calculatorDetail.dpPercentage;
      this.tenureValue = this.calculatorDetail.term;
    });
  }
  
  getECalculatorDetail(recaptchaToken: string, isDefault: boolean = true) {
    const calculatorFor: ECalculatorRequestViewModel = new ECalculatorRequestViewModel();
    calculatorFor.variantId = this.selectedVariant.id; // 148932;
    calculatorFor.captchaToken = recaptchaToken;
    calculatorFor.source = this.source;

    if (!isDefault) {
      calculatorFor.dPPercentage = this.dpValue;
      calculatorFor.financingProduct = this.selectedFinancingProduct;
      calculatorFor.mileage = this.selectedMileage;
      calculatorFor.term = this.tenureValue;

      // For shop.bmw.com.my // Added by Ahsan
    } else if (this.selectedVariant && this.selectedVariant.isShopEnable) {
      calculatorFor.financingProduct = 'StraightLine';
      calculatorFor.term = 36;
    }
    calculatorFor.assetType = this.assetType;
    //For Dealer
    if (this.configurationDataService.source == 'CFEDealer') {
      calculatorFor.source = this.configurationDataService.source;
      calculatorFor.assetCost = this.AssetCost;
    }

    return new Promise<void>(resolve => {
      this.eCalculatorService.getInstallment(calculatorFor).subscribe(calculatorDetail => {
        if (calculatorDetail) {
          this.calculatorDetail = calculatorDetail;
        }
        else
          this.calculatorDetail = new ECalculatorResponseViewModel();
        resolve();
      });
    });
  }

  public financing(financingProduct) {
    this.selectedFinancingProduct = financingProduct;
    this.populateCalculatorDetail('financeStep', false);
  }
  
  public financeStep() {
    this.currentStep = "2";
    this.data.changeMessage("Step " + this.currentStep);
  }

  onEmail() {    

    if (this.customerEmail && this.customerName)
    {
      const event = new CustomEvent("engageEvent", {detail: this.customerName})
      window.dispatchEvent(event)

      const customerCarComparison = new CustomerCarComparison();
      customerCarComparison.CalculationId = 0;
      customerCarComparison.EmailAddress = this.customerEmail;
      customerCarComparison.FullName = this.customerName;
      customerCarComparison.PreferredDealerId = 45;
      customerCarComparison.PreferredStateId = 66;
      customerCarComparison.ProductType = 1;
      customerCarComparison.RequestedURL = "Configurator";
      customerCarComparison.steps = 0;
      customerCarComparison.UserGuid = "";
      customerCarComparison.ReferenceId = "";
      customerCarComparison.PhoneNumber = "";
  
      let ecalcDetails = new CarComparison();
      ecalcDetails.AmountFinance  = this.calculatorDetail.amountFinance
      ecalcDetails.AssetCost = this.calculatorDetail.assetCost;
      ecalcDetails.AssetType = this.calculatorDetail.assetType;
      ecalcDetails.CalculationId = 0;
      ecalcDetails.CampaignExpiryDate = this.calculatorDetail.campaignExpiryDate;
      ecalcDetails.CampaignModelId = this.calculatorDetail.campaignModelId;
      ecalcDetails.DownPayment = this.calculatorDetail.downPayment;
      ecalcDetails.DownPaymentPercentage = this.calculatorDetail.downPaymentPercentage;
      ecalcDetails.EmailAddress = "";
      ecalcDetails.FinancingProduct = this.calculatorDetail.financingProduct;
      ecalcDetails.FullName = "";
      ecalcDetails.GFV = this.calculatorDetail.gfv;
      ecalcDetails.InterestRate = this.calculatorDetail.rate;
      ecalcDetails.LastInstallment = this.calculatorDetail.lastMonthInstallment;
      ecalcDetails.MarketingConsent = false;
      ecalcDetails.MonthlyInstallment = this.calculatorDetail.monthlyInstallment;
      ecalcDetails.OptionNo = this.calculatorDetail.optionNo;
      ecalcDetails.PreferredDealerId = 0;
      ecalcDetails.PreferredStateId = 0;
      ecalcDetails.RRP = this.calculatorDetail.rrp;
      ecalcDetails.RV = this.calculatorDetail.rv;
      ecalcDetails.RequestedURL = "Configurator";
      ecalcDetails.Tenure = this.calculatorDetail.term;
      ecalcDetails.UniqueId = "";
      ecalcDetails.ValidityDate = this.calculatorDetail.validityDate;
      ecalcDetails.VariantId = this.calculatorDetail.variantId; 
      
      customerCarComparison.ECalculatorDetail = <Array<CarComparison>> new Array(ecalcDetails);
      return new Promise<void>(resolve => {
        this.eCalculatorService.saveCustomerCarComparison(customerCarComparison).subscribe(resultViewModel => {
          this.resultViewModel = resultViewModel;
          resolve();
        });
      });
    }
    else
    {
      console.log("empty input");
    }
  }
}

