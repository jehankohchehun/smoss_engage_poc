import { Component, ChangeDetectionStrategy, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { LoaderService } from 'src/app/_services/loader.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'bmw-engage-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoaderComponent implements AfterViewInit {

  color = 'primary';
  mode = 'indeterminate';
  value = 50;
  isLoading: Subject<boolean> = this.loaderService.isLoading;

  constructor(private loaderService: LoaderService, private ref: ChangeDetectorRef) { }

  ngAfterViewInit(): void {
    this.ref.detectChanges();
  }
}