export const environment = {
  production: false,

  CFE_ENDPOINT: 'https://cfeapi-test.bmwfs.my/api/',
  //CFE_ENDPOINT:'https://localhost:44392/api/',
  NFSC_ENDPOINT: 'https://nfsccoreapi-test.bmwfs.my/api/',
  //NFSC_ENDPOINT: 'https://localhost:44383/api/',

  UI_ENDPOINT: "https://cfe-uat.bmwfs.my",

  MOBILE_RETURN_URL: "bmw://bigledger.wavelet.bmw.crm/finance?src:direct-debit",
  COMPARISON_URL: "Comparison",
  CONFIGURATOR_URL: "Configurator",
  ECALCULATOR_PERSONAL_DETAIL_URL: "ECalculatorPersonalDetail",
  MOBILE_LOGIN_RETURN_URL: "bmw://bigledger.wavelet.bmw.crm/login",
  SSP_LOGIN_RETURN_URL: "https://cfe-uat.bmwfs.my/authentication/login",

  EMANDATE_URL: '/emandate/summary?token=',
  ALLIANZ_URL: 'https://getquoteuat.allianz.com.my/motor-online?utm_source=BMWCRE&utm_medium=APP&utm_campaign=ENGAGE',
  RecaptchaKey: '6Lf-QcYUAAAAAL8tkSSzCExYtbpBmW8xiAGHUTJW',
  UILogEnabled:true,
  LocRocketEnabled:true,
  LocRocketId:"fibuxy/bmwengage_uat",
  Source: "CFE"
};